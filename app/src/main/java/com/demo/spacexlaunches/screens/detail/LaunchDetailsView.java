package com.demo.spacexlaunches.screens.detail;

import android.util.Pair;

import com.demo.spacexlaunches.db.model.SpaceX;
import com.google.android.youtube.player.YouTubePlayer;

import io.reactivex.Observable;

public interface LaunchDetailsView {

    void backPressed();

    void setupView(SpaceX spaceX);

    void openWiki(String wikiLink);

    Observable<Pair<YouTubePlayer, Boolean>> initializeYoutubePlayer();

    void playVideo(String youtubeId, YouTubePlayer youTubePlayer, boolean wasRestored);

    Observable<Boolean> detectFullscreen();

    Observable<Object> launchImageClicked();

    Observable<Object> backButtonClicked();

    Observable<Object> linkClicked();
}
