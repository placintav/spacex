package com.demo.spacexlaunches.screens.detail.di;

import com.demo.spacexlaunches.screens.detail.LaunchDetailsPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LaunchDetailsModule {

    @Provides
    LaunchDetailsPresenter providePresenter() {
        return new LaunchDetailsPresenter();
    }

}
