package com.demo.spacexlaunches.screens.main;

import com.demo.spacexlaunches.db.dao.SpaceXDao;
import com.demo.spacexlaunches.db.model.SpaceX;
import com.demo.spacexlaunches.net.ConnectionState;
import com.demo.spacexlaunches.net.SpaceXApi;
import com.demo.spacexlaunches.net.models.Links;
import com.demo.spacexlaunches.net.models.Rocket;
import com.demo.spacexlaunches.net.models.Space;
import com.demo.spacexlaunches.rx.RxSchedulers;
import com.demo.spacexlaunches.screens.base.BasePresenter;
import com.demo.spacexlaunches.utils.DateUtil;
import com.demo.spacexlaunches.utils.Parse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class MainPresenter extends BasePresenter<MainView> {

    private SpaceXApi api;
    private SpaceXDao dao;
    private ConnectionState connectionState;
    private RxSchedulers rxSchedulers;

    public MainPresenter(SpaceXApi api, SpaceXDao dao, ConnectionState connectionState, RxSchedulers rxSchedulers) {
        this.api = api;
        this.dao = dao;
        this.connectionState = connectionState;
        this.rxSchedulers = rxSchedulers;
    }

    @Override
    protected void onLoad() {
        super.onLoad();
        disposables.add(loadLaunches());
    }

    private Disposable loadLaunches() {
        return Observable.just(connectionState.isOnline())
                .flatMap(isOnline -> {
                    if (isOnline) {
                        return api.getAllPastLaunches();
                    } else {
                        return Observable.just(new ArrayList<Space>());
                    }
                })
                .flatMap(spaces -> {
                    if (!spaces.isEmpty()) {
                        List<SpaceX> spaceXList = new ArrayList<>();
                        for (Space space : spaces) {
                            Rocket rocket = space.getRocket();
                            Links links = space.getLinks();

                            SpaceX spaceX = new SpaceX(space.getFlightNumber(),
                                    space.getMissionName(),
                                    space.getDetails(),
                                    DateUtil.convertTimestampToFormattedDate(space.getLaunchDateUnix() * 1000),
                                    rocket.getRocketName(),
                                    rocket.getSecondStage().getPayloads().get(0).getPayloadMassKg(),
                                    links.getWikipedia(),
                                    Parse.getYoutubeId(links.getVideoLink()));

                            spaceXList.add(spaceX);
                        }

                        dao.insert(spaceXList);
                    }

                    return Observable.just(1);
                })
                .subscribeOn(rxSchedulers.io())
                .observeOn(rxSchedulers.androidUI())
                .subscribe(success -> getView().showLaunches());
    }
}
