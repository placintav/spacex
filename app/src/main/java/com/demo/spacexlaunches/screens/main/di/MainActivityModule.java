package com.demo.spacexlaunches.screens.main.di;

import com.demo.spacexlaunches.db.dao.SpaceXDao;
import com.demo.spacexlaunches.net.ConnectionState;
import com.demo.spacexlaunches.net.SpaceXApi;
import com.demo.spacexlaunches.rx.RxSchedulers;
import com.demo.spacexlaunches.screens.main.MainPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    MainPresenter providePresenter(SpaceXApi api, SpaceXDao dao, ConnectionState connectionState, RxSchedulers rxSchedulers) {
        return new MainPresenter(api, dao, connectionState, rxSchedulers);
    }

}
