package com.demo.spacexlaunches.screens.main;

public interface MainView  {
    void showLaunches();
}
