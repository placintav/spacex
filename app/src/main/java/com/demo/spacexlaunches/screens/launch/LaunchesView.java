package com.demo.spacexlaunches.screens.launch;

import com.demo.spacexlaunches.db.model.SpaceX;

import java.util.List;

import io.reactivex.Observable;

public interface LaunchesView {

    void displayLaunches(List<SpaceX> launches);

    void openDetails(SpaceX spaceX);

    Observable<SpaceX> launchClicked();
}
