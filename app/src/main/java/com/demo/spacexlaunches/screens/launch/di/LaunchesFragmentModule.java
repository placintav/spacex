package com.demo.spacexlaunches.screens.launch.di;

import com.demo.spacexlaunches.db.dao.SpaceXDao;
import com.demo.spacexlaunches.rx.RxSchedulers;
import com.demo.spacexlaunches.screens.launch.LaunchesPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class LaunchesFragmentModule {

    @Provides
    LaunchesPresenter providePresenter(SpaceXDao spaceXDao, RxSchedulers rxSchedulers) {
        return new LaunchesPresenter(spaceXDao, rxSchedulers);
    }

}
