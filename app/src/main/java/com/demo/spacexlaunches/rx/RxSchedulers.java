package com.demo.spacexlaunches.rx;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RxSchedulers {

    private static final Executor NETWORK_EXECUTOR = Executors.newCachedThreadPool();
    private static final Scheduler NETWORK_SCHEDULER = Schedulers.from(NETWORK_EXECUTOR);


    public Scheduler androidUI() {
        return AndroidSchedulers.mainThread();
    }


    public Scheduler io() {
        return Schedulers.io();
    }


    public Scheduler computation() {
        return Schedulers.computation();
    }


    public Scheduler network() {
        return NETWORK_SCHEDULER;
    }


    public Scheduler trampoline() {
        return Schedulers.trampoline();
    }
}
