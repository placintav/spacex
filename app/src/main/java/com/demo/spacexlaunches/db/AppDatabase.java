package com.demo.spacexlaunches.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.demo.spacexlaunches.db.dao.SpaceXDao;
import com.demo.spacexlaunches.db.model.SpaceX;

@Database(entities = {SpaceX.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DB_NAME = "space_x.db";

    public static AppDatabase initDatabase(Context context) {
        return Room.databaseBuilder(
                context.getApplicationContext(),
                AppDatabase.class,
                DB_NAME).build();
    }

    public abstract SpaceXDao spaceXDao();

}
