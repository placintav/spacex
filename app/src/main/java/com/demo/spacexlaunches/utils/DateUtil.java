package com.demo.spacexlaunches.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    public static String convertTimestampToFormattedDate(Long timestamp) {
        java.util.Date date = new java.util.Date(timestamp);
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE dd, yyyy", Locale.getDefault());
        return formatter.format(date);
    }

}
