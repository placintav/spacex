package com.demo.spacexlaunches.app.di;

import android.content.Context;

import com.demo.spacexlaunches.db.AppDatabase;
import com.demo.spacexlaunches.db.dao.SpaceXDao;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule {

    @Provides
    AppDatabase provideAppDatabase(Context context) {
        return AppDatabase.initDatabase(context);
    }

    @Provides
    SpaceXDao provideSpaceXDao(AppDatabase appDatabase) {
        return appDatabase.spaceXDao();
    }


}
