package com.demo.spacexlaunches.app;

import android.app.Activity;
import android.app.Application;

import com.demo.spacexlaunches.app.di.AppComponent;
import com.demo.spacexlaunches.app.di.AppModule;
import com.demo.spacexlaunches.app.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

public class SpaceXApp extends Application implements HasActivityInjector {

    private static AppComponent appComponent;
    @Inject
    DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(this))
                .build();

        appComponent.inject(this);
    }


    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }
}
