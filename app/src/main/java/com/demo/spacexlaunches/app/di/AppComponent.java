package com.demo.spacexlaunches.app.di;

import com.demo.spacexlaunches.app.SpaceXApp;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;

@Component(modules = {
        AndroidInjectionModule.class,
        AndroidSupportInjectionModule.class,
        AppModule.class,
        NetworkModule.class,
        DatabaseModule.class,
        BuildersModule.class
})
public interface AppComponent {

    void inject(SpaceXApp application);

}
