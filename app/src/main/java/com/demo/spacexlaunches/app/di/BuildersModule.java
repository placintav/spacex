package com.demo.spacexlaunches.app.di;

import com.demo.spacexlaunches.screens.detail.LaunchDetailsFragment;
import com.demo.spacexlaunches.screens.detail.di.LaunchDetailsModule;
import com.demo.spacexlaunches.screens.launch.LaunchesFragment;
import com.demo.spacexlaunches.screens.launch.di.LaunchesFragmentModule;
import com.demo.spacexlaunches.screens.main.MainActivity;
import com.demo.spacexlaunches.screens.main.di.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity bindSpaceActivity();

    @ContributesAndroidInjector(modules = {LaunchesFragmentModule.class})
    abstract LaunchesFragment bindLaunchesFragment();

    @ContributesAndroidInjector(modules = {LaunchDetailsModule.class})
    abstract LaunchDetailsFragment bindLaunchDetailsFragment();

}
