package com.demo.spacexlaunches.net;

public class Constants {

    public static final String YOUTUBE_IMG_BASE_URL = "http://img.youtube.com/vi/";
    public static final String YOUTUBE_IMG_END_URL = "/0.jpg";

    public static final String SPACE_X_KEY = "space_x_key";

    public static final String YOUTUBE_API_KEY = "AIzaSyAaO8mJ-2AXGu-YbaNAqwo1wQw9ffQ7-3c";

    public static final int TIME_BETWEEN_CLICKS = 500;
}
