package com.demo.spacexlaunches.net;

import com.demo.spacexlaunches.net.models.Space;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface SpaceXApi {
    @GET("v2/launches")
    Observable<List<Space>> getAllPastLaunches();
}
